import requests

server_url = "http://localhost:8000/process_video"

video_url = "https://www.youtubepi.com/watch?v=XXYlFuWEuKI"

response = requests.post(server_url, json={"video_url": video_url})

if response.status_code == 200:
    result_url = response.json()["result_url"]
    print("Result URL:", result_url)
else:
    print("Error:", response.text)
