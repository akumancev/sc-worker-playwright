import asyncio
from fastapi import FastAPI, HTTPException, Body
from playwright.async_api import async_playwright
from playwright_stealth import stealth_async

app = FastAPI()

async def process_video_url(video_url):
    async with async_playwright() as playwright:
        browser = await playwright.chromium.launch()
        context = await browser.new_context()
        page = await context.new_page()
        await stealth_async(page)
        await page.goto(video_url)
        await page.wait_for_load_state("networkidle")
        await page.click("#btnSubmit")
        await asyncio.sleep(1)
        await page.click('button:has-text("Audio")')
        await asyncio.sleep(1)
        await page.wait_for_load_state("networkidle")
        await page.click('td#btn128 button.btn')
        await asyncio.sleep(1)
        href_value = await page.evaluate('(document.querySelector("td#btn128 button.btn a")).getAttribute("href")')
        await browser.close()
        return href_value

@app.get("/healthcheck")
async def health_check():
    return {"status": "OK"}

@app.post("/process_video")
async def process_video(video_url: str = Body(..., embed=True)):
    try:
        result_url = await process_video_url(video_url)
        return {"result_url": result_url}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# import asyncio
# from playwright.async_api import async_playwright
# from playwright_stealth import stealth_async
 
# async def main():
#     # Launch the Playwright instance
#     async with async_playwright() as playwright:
#         # Launch the browser
#         browser = await playwright.chromium.launch()
 
#         # Create new context
#         context = await browser.new_context()
        
#         # Create new page within the context
#         page = await context.new_page()
 
#         # Apply stealth to the page
#         await stealth_async(page)
 
#         # Navigate to the desired URL
#         await page.goto("https://www.youtubepi.com/watch?v=XXYlFuWEuKI")
 
#         # Wait for any dynamic content to load
#         await page.wait_for_load_state("networkidle")

#         await page.click("#btnSubmit")

#         await asyncio.sleep(1)

#         await page.click('button:has-text("Audio")')

#         await asyncio.sleep(1)

#         await page.wait_for_load_state("networkidle")

#         # Click the "Convert" button
#         await page.click('td#btn128 button.btn')

#         await asyncio.sleep(1)

#         href_value = await page.evaluate('(document.querySelector("td#btn128 button.btn a")).getAttribute("href")')
        
#         # Print the href value to the console
#         print("Download link:", href_value)
 
#         # Take a screenshot of the page
#         # await page.screenshot(path="download_button.png")
#         # print("Screenshot captured successfully.")
 
#         # Close the browser
#         await browser.close()
 
# # Run the main function
# asyncio.run(main())
